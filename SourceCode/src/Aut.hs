{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

module Aut
  ( Aut(..)
  , State(..)
  , show
  , after
  , computeCompRel
  , inp
  , out
  , enab
  , trans
  , initial
  , states
  , inputs
  , outputs
  , outSet
  , afterSet
  , statesToAut
  , afterSequence
  , inSet
  , getAccesSequences
  , getTransitionExtendedAccesSequences
  , union
  , constrAut
  , printCompRel
  , getDistingCompPairs
  ) where

import qualified Control.DeepSeq               as DeepSeq
import qualified Control.Monad
import qualified Data.Bifunctor
import           Data.Char                      ( GeneralCategory(Control) )
import           Data.Foldable                  ( Foldable(foldMap, foldMap')
                                                , foldlM
                                                )
import qualified Data.Function
import qualified Data.List                     as List
import           Data.Map                      as Map
                                                ( (!)
                                                , Map
                                                )
import qualified Data.Map                      as Map
import qualified Data.Maybe                    as Maybe
import qualified Data.Serialize                as Serialize
import           Data.Set                      as Set
                                                ( Set )
import qualified Data.Set                      as Set
import           Debug.Trace                   as Trace
import qualified GHC.Generics                  as Generics

data Aut a b = Aut
  { initial    :: State a b
  , states     :: Set (State a b)
  , idStateMap :: Map a (State a b)
  , inputs     :: Set b
  , outputs    :: Set b
  }
  deriving (Generics.Generic, DeepSeq.NFData)

instance (Serialize.Serialize a, Serialize.Serialize b, Ord a, Ord b)
  => Serialize.Serialize (Aut a b)

instance (Show a, Show b) => Show (Aut a b) where
  show (Aut initial states map inps outs) =
    "Initial: "
      ++ show initial
      ++ "\n"
      ++ "States: "
      ++ show (Set.toList states)
      ++ "\n"
      ++ "Input alphabet:"
      ++ show inps
      ++ "Output alphabet:"
      ++ show outs

--"IdStateMap: " ++ (show $ (Map.mapKeys Util.stateToName . Map.map (Util.stateToName . sid)) map)
data State a b = State
  {
    -- | @State@ id.
    sid   :: a
  ,
    -- | Inputs defined at @State@.
    inp   :: Set b
  ,
    -- | Outputs defined at @State@.
    out   :: Set b
  ,
    -- | Outgoing transitions using inputs/outputs to a @State@.
    trans :: Map b a
  }
  deriving (Ord, Generics.Generic, DeepSeq.NFData)

instance ((Show a) => Show (State a b)) where
  show = show . sid

instance (Serialize.Serialize a, Serialize.Serialize b, Ord a, Ord b)
  => Serialize.Serialize (State a b)

instance (Eq a) => Eq (State a b) where
  -- (==) s1 s2 = sid s1 == sid s2
  (==) = (==) `Data.Function.on` sid

statesToAut :: (Ord a, Ord b) => State a b -> Set (State a b) -> Aut a b
statesToAut ini states =
  let (map, inps, outs) = Set.foldr
        (\s (map, inps, outs) ->
          (Map.insert (sid s) s map, inps <> inp s, outs <> out s)
        )
        (Map.empty, Set.empty, Set.empty)
        states
  in  Aut ini states map inps outs

-- | Collect all enabled inputs and outputs at a state.
enab :: Ord b => State a b -> Set b
enab s = inp s <> out s

-- | Collect all enabled outputs at a state.
outSet :: Ord b => Set (State a b) -> Set b
outSet = Data.Foldable.foldMap out

-- | Collect all enabled inputs at a state.
inSet :: Ord b => Set (State a b) -> Set b
inSet = Data.Foldable.foldMap inp

after :: (Ord a, Ord b) => State a b -> b -> Aut a b -> Maybe (State a b)
after state mu aut =
  Map.lookup mu (trans state) >>= flip Map.lookup (idStateMap aut)

afterSet
  :: (Ord a, Ord b) => Set (State a b) -> b -> Aut a b -> Set (State a b)
afterSet stateSet mu aut =
  Set.fromList $ Maybe.mapMaybe (\s -> after s mu aut) (Set.toList stateSet)

-- afterSequence
--   :: (Ord a, Ord b) => State a b -> [b] -> Aut a b -> Maybe (State a b)
-- afterSequence state []         aut = Just state
-- afterSequence state (mu : mus) aut = case after state mu aut of
--   Nothing -> Nothing
--   Just s  -> afterSequence s mus aut

-- | Given an initial state, a sequence, and the automaton, return the final state.
afterSequence
  :: (Ord a, Ord b)
  => Aut a b -- ^ Automaton
  -> State a b -- ^ Initial state.
  -> [b] -- ^ Sequence.
  -> Maybe (State a b) -- ^ Final state.
afterSequence aut = foldlM (\s b -> after s b aut)

computeCompRel
  :: (Ord a, Ord b, Show b, Show a) => Aut a b -> Set (State a b, State a b)
computeCompRel aut = computeCompRelAbstract aut initialCompRel expandCompRel

initialCompRel :: (Ord a, Ord b) => Aut a b -> Set (State a b, State a b)
initialCompRel aut = Set.cartesianProduct (states aut) (states aut)

expandCompRel
  :: (Ord a, Ord b, Show a)
  => Aut a b
  -> Set (State a b, State a b)
  -> Set (State a b, State a b)
expandCompRel aut rel =
  let qs = Set.toList $ states aut
      match f q q' = Set.intersection (f q) (f q')
      all_match = match inp
      any_match = match out
  in  Set.fromList
        [ (q, q')
        | q  <- qs
        , q' <- qs
        , let mem = compMemFunc aut rel q q'
        , all mem (all_match q q') && any mem (any_match q q')
        ]

-- | This stands for "compute member function", maybe??
compMemFunc
  :: (Ord a, Ord b, Show a)
  => Aut a b
  -> Set (State a b, State a b)
  -> State a b
  -> State a b
  -> b
  -> Bool
compMemFunc aut rel q q' c =
  let dest x = Maybe.fromJust $ after x c aut
  in  Set.member (dest q, dest q') rel

computeCompRelAbstract
  :: (Ord a, Ord b, Eq c, Show c, Show b)
  => Aut a b
  -> (Aut a b -> c)
  -> (Aut a b -> c -> c)
  -> c
computeCompRelAbstract aut firstAbstract expand =
  let first  = firstAbstract aut
      second = expand aut first
  in  computeCompRecAbstract first second (expand aut)

computeCompRecAbstract :: (Eq c, Show c) => c -> c -> (c -> c) -> c
computeCompRecAbstract first second f =
  if first == second then first else computeCompRecAbstract second (f second) f

-- | Get all access sequences of an `Aut`.
getAccesSequences :: (Ord a, Ord b) => Aut a b -> [[b]]
getAccesSequences aut = Map.elems $ getAccesSequences'
  aut
  (Set.singleton $ Aut.initial aut)
  (Map.singleton (Aut.initial aut) [])

-- | Implementation of the @getAccessSequences@ function.
getAccesSequences'
  :: (Ord a, Ord b)
  => Aut a b -- ^Automaton.
  -> Set (State a b) -- ^Set of states to investigate.
  -> Map (State a b) [b] -- ^Current access map.
  -> Map (State a b) [b] -- ^Final access map.
getAccesSequences' aut toInv accMap = if Set.null toInv
  then accMap -- If no more elements are left, return the map.
  else
    let state                 = Set.elemAt 0 toInv
        (newToInv, newAccMap) = List.foldr
          (\(mu, dest) (inv, map) ->
            let destState       = Aut.idStateMap aut ! dest
                destStateAccSeq = (map ! state) ++ [mu]
            in  if Map.notMember destState map
                  then
                    ( Set.insert destState inv
                    , Map.insert destState destStateAccSeq map
                    )
                  else (inv, map)
          )
          (Set.delete state toInv, accMap) -- `Initial` element is just an empty set and empty access map.
          (Map.toList (Aut.trans state)) -- Gives all transitions of `state` in a list.
    in  getAccesSequences' aut newToInv newAccMap


-- | Extend a sequence with all inputs and outputs defined at the destination state.
extendAccWithTransition :: (Ord a, Ord b) => Aut a b -> [b] -> Set [b]
extendAccWithTransition aut acc =
  let state = Maybe.fromJust $ afterSequence aut (initial aut) acc
  in  Set.map (\mu -> acc ++ [mu]) (enab state)

-- | Extend each access sequence of the automaton with the actions defined at their respective
-- destinations.
getTransitionExtendedAccesSequences :: (Ord a, Ord b) => Aut a b -> [[b]]
getTransitionExtendedAccesSequences aut =
  let access_seqs = Set.fromList $ getAccesSequences aut
  in  Set.toList
        $  access_seqs
        <> Data.Foldable.foldMap (extendAccWithTransition aut) access_seqs

-- | Combine two @Aut@ iff their state identifiers are disjoint.
-- Use the initial state of the first as the new initial state.
union
  :: (Ord a, Ord b)
  => Aut a b -- ^ First @Aut@
  -> Aut a b -- ^ Second @Aut@
  -> Aut a b -- ^ Combined @Aut@
union aut1 aut2 = if Set.disjoint (states aut1) (states aut2)
  then
    let combine f = f aut1 <> f aut2
    in  Aut (initial aut1)
            (combine states)
            (combine idStateMap)
            (combine inputs)
            (combine outputs)
  else error "states identifiers of automata not disjunct"

constrAut
  :: (Ord a, Ord b, Show a, Show b)
  => (a, Set (a, b, a), Set b, Set b) -- ^ Initial state id, Set of transitions, and input and output sets.
  -> Aut a b
constrAut (initial, transs, inps, outs) =
  let statemap = stautToStateMap transs inps outs
      noTransStates =
        [ t | (f, mu, t) <- Set.toList transs, Map.notMember t statemap ]
      fullStateMap = List.foldr
        (\s m -> Map.insert s (Set.empty, Set.empty, Map.empty) m)
        statemap
        noTransStates
  in  case Map.lookup initial fullStateMap of
        Nothing -> error "Initial state does not have any transitions"
        Just (ini, outi, tmapi) ->
          let statesandmap = getStatesAndMap fullStateMap
          in  uncurry (Aut (State initial ini outi tmapi))
                      statesandmap
                      inps
                      outs
getStatesAndMap
  :: (Ord a, Ord b)
  => Map a (Set b, Set b, Map b a)
  -> (Set (State a b), Map a (State a b))
-- acc is the accumulator
getStatesAndMap = Map.foldlWithKey
  (\acc key (ins, outs, tmaps) ->
    let state = State key ins outs tmaps
    in  Data.Bifunctor.bimap (Set.insert state) (Map.insert key state) acc
  )
  (Set.empty, Map.empty)


stautToStateMap
  :: (Ord a, Ord b, Show a, Show b)
  => Set (a, b, a)
  -> Set b
  -> Set b
  -> Map a (Set b, Set b, Map b a)
stautToStateMap transs inps outs = Set.foldl
  (\m t -> case t of
    (f, mu, t) -> if Set.member mu inps
      then Map.insertWith (mergeMaps f)
                          f
                          (Set.singleton mu, Set.empty, Map.singleton mu t)
                          m
      else if Set.member mu outs
        then Map.insertWith (mergeMaps f)
                            f
                            (Set.empty, Set.singleton mu, Map.singleton mu t)
                            m
        else error ("Channel " ++ show mu ++ " neither input nor output!") -- ++ (show (f, mu, t)))
  )
  Map.empty
  transs

mergeMaps
  :: (Ord b, Show a, Show b)
  => a
  -> (Set b, Set b, Map b a)
  -> (Set b, Set b, Map b a)
  -> (Set b, Set b, Map b a)
mergeMaps f (ni, no, nm) (oi, oo, om) =
  let (c, s) = head $ Map.toList nm
  in  case Map.lookup c om of
        Nothing -> (Set.union ni oi, Set.union no oo, Map.insert c s om)
        Just d  -> error
          (  "stautdef nondeterministic!\n"
          ++ show f
          ++ " -> "
          ++ show c
          ++ " -> "
          ++ show d
          ++ " AND "
          ++ show f
          ++ " -> "
          ++ show c
          ++ " -> "
          ++ show s
          )

printCompRel :: (Show a, Eq a) => Set (State a b, State a b) -> String
printCompRel compRel = List.intercalate
  "\n"
  (Set.toList $ Set.map
    (\t ->
      "(s" ++ show (Aut.sid $ fst t) ++ ", s" ++ show (Aut.sid $ snd t) ++ ")"
    )
    (Set.filter (uncurry (/=)) compRel)
  )

getDistingCompPairs
  :: (Ord a, Ord b) => Aut a b -> Set (State a b, State a b) -> [b] -> Int
getDistingCompPairs aut comp sigma = List.length
  [ (q1, q2)
  | let dest x = afterSequence aut x sigma
  , (q1, q2) <- Set.toList comp
  , case (dest q1, dest q2) of
    (Just _ , Nothing) -> True
    (Nothing, Just _ ) -> True
    _                  -> False
  ]
