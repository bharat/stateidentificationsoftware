module InputSelection
  ( getAlfSelection
  , InputSelection
  ) where

import           Data.ByteString.Char8          ( ByteString )
import qualified Data.ByteString.Char8         as B

type InputSelection
  = ByteString -> ByteString -> ByteString -> ByteString -> Bool

init1Inputs :: [String]
init1Inputs = ["21.1", "21.0", "22", "53.4", "52.5"]

init2Inputs :: [String]
init2Inputs = ["6.0", "25.0.0", "10", "11", "37.2"]

error1Inputs :: [String]
error1Inputs = ["13.1", "14", "13.0"]

error2Inputs :: [String]
error2Inputs = ["24.0", "39", "17"]

idleInputs :: [String]
idleInputs = ["18", "37.0", "53.0", "9.2"]

sleep1Inputs :: [String]
sleep1Inputs = ["19", "46", "9.4"]

sleep2Inputs :: [String]
sleep2Inputs = ["20", "37.3", "31.0"]

standby1Inputs :: [String]
standby1Inputs = ["26.0", "27.0", "9.1"]

standby2Inputs :: [String]
standby2Inputs = ["30.0", "45"]

standby3Inputs :: [String]
standby3Inputs = ["28.0", "36.0", "43", "51"]

lowPowerInputs :: [String]
lowPowerInputs = ["9.3", "29.0", "44"]

running1Inputs :: [String]
running1Inputs = ["32.0", "33.0", "3.0"]

running2Inputs :: [String]
running2Inputs = ["15", "16", "34.0", "49"]

stoppingInputs :: [String]
stoppingInputs = ["35.0", "50", "3.1"]

extraInputs :: [String]
extraInputs =
  [ "9.5"
  , "7"
  , "23.0"
  , "13.2"
  , "13.3"
  , "3.5"
  , "1"
  , "2.5.5"
  , "0.0"
  , "4"
  , "5"
  , "38"
  , "40.0"
  , "42"
  , "41"
  , "48"
  , "47"
  , "52.0"
  , "52.1"
  , "52.2"
  , "52.3"
  , "52.4"
  , "53.1"
  , "53.2"
  , "53.3"
  , "53.5"
  , "37.1"
  , "37.4"
  , "37.5"
  , "12"
  ]

initAndError :: InputSelection
initAndError s i o t =
  B.unpack i `elem` init1Inputs ++ init2Inputs ++ error1Inputs ++ error2Inputs

initIdleAndSleep :: InputSelection
initIdleAndSleep s i o t =
  B.unpack i
    `elem` init1Inputs
    ++     init2Inputs
    ++     idleInputs
    ++     sleep1Inputs
    ++     sleep2Inputs

initIdleStandbySleep :: InputSelection
initIdleStandbySleep s i o t =
  B.unpack i
    `elem` init1Inputs
    ++     init2Inputs
    ++     idleInputs
    ++     standby1Inputs
    ++     standby2Inputs
    ++     standby3Inputs
    ++     sleep1Inputs
    ++     sleep2Inputs

initIdleStandbyRunning :: InputSelection
initIdleStandbyRunning s i o t =
  B.unpack i
    `elem` init1Inputs
    ++     init2Inputs
    ++     idleInputs
    ++     standby1Inputs
    ++     standby2Inputs
    ++     standby3Inputs
    ++     running1Inputs
    ++     running2Inputs

initIdleStandbyLowPower :: InputSelection
initIdleStandbyLowPower s i o t =
  B.unpack i
    `elem` init1Inputs
    ++     init2Inputs
    ++     idleInputs
    ++     standby1Inputs
    ++     standby2Inputs
    ++     standby3Inputs
    ++     lowPowerInputs

full :: InputSelection
full s i o t = True

getAlfSelection :: String -> InputSelection
getAlfSelection "ESM-IE"    = initAndError
getAlfSelection "ESM-IISl"  = initIdleAndSleep
getAlfSelection "ESM-IISSl" = initIdleStandbySleep
getAlfSelection "ESM-IISR"  = initIdleStandbyRunning
getAlfSelection "ESM-IISL"  = initIdleStandbyLowPower
getAlfSelection "full"      = full
getAlfSelection n           = error $ n ++ " is no alphabet!"
