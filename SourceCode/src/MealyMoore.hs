{-# LANGUAGE RecordWildCards #-}

module MealyMoore where

import qualified Data.Maybe                    as Maybe
{- Author: Joshua Moerman -}
{-
We implement Moore and Mealy machines (co)algebraically as functions. This way
it is very generic. Note that the state space has to be encoded separately if
used in algorithms (e.g., minimisation needs this). Unfortunately, we cannot
have duplicate record fields in Haskell (there is an extension which is not
expressive enough). Hence, the fields of Mealy are appended with `T` for
transducer.
-}
import           Prelude                 hiding ( init )

-- Deterministic automaton with input alphabet i and outputs o
data Moore s i o = Moore
  { init  :: s
  , out   :: s -> o
  , delta :: s -> i -> s
  }

-- |A Mealy machines has outputs on transitions
data Mealy s i o = Mealy
  { initT  :: s
  , deltaT :: s -> i -> (s, o)
  }

-- A DFA is a Moore machine with a boolean as output (accepting/rejecting)
type DFA s i = Moore s i Bool

-- Functor instances to map over the output
instance Functor (Moore s i) where
  fmap f Moore { out = oldOut, ..} = let out = f . oldOut in Moore { .. }

instance Functor (Mealy s i) where
  fmap f Mealy { deltaT = oldBeh, ..} =
    let deltaT s i = fmap f (oldBeh s i) in Mealy { .. }

{-
NOTE: an applicative instance exists if we abstract away `s`, which is not
great. Instead, we simply give a product construction. This fits most needs.
Combines two automata on the same alphabet by running them in parallel. Output
is combined with a function.
-}
productWith
  :: (o1 -> o2 -> o3) -> Moore s i o1 -> Moore t i o2 -> Moore (s, t) i o3
productWith f m1 m2 = Moore { init  = (init m1, init m2)
                            , out   = \(s, t) -> f (out m1 s) (out m2 t)
                            , delta = \(s, t) i -> (delta m1 s i, delta m2 t i)
                            }

-- Product automata which outputs tuples
product :: Moore s i o1 -> Moore t i o2 -> Moore (s, t) i (o1, o2)
product = productWith (,)

-- The usual boolean combinators for languages: intersection, union, difference
intersectionDFA :: DFA s i -> DFA t i -> DFA (s, t) i
intersectionDFA = productWith (&&)

unionDFA :: DFA s i -> DFA t i -> DFA (s, t) i
unionDFA = productWith (||)

differenceDFA :: Eq o => Moore s i o -> Moore t i o -> DFA (s, t) i
differenceDFA = productWith (/=)

{-
The (symmetric) difference for a Mealy machine is a bit different. Here a word
is accepted (in the difference) if it gives a different output on one of its
prefixes. This makes sense for reactive systems. Hence, the language is a co-
safety language. The accepting sink-state is encoded as the state `Nothing`.
-}
differenceDFAPrefix
  :: Eq o => Mealy s i o -> Mealy t i o -> DFA (Maybe (s, t)) i
differenceDFAPrefix m1 m2 = Moore
  { init  = Just (initT m1, initT m2)
  , out   = Maybe.isNothing
  , delta = \st i ->
              (\(s, t) ->
                  let (s2, o1) = deltaT m1 s i
                      (t2, o2) = deltaT m2 t i
                  in  if o1 == o2 then Just (s2, t2) else Nothing
                )
                =<< st
  }

{-
We can always convert between Moore and Mealy if the output alphabet is given by
functions (i -> o). The conversion is basically currying. Here we define
conversion function from Moore to Mealy and back. We have the following laws:

  toMealy . toMoore = id
  toMoore . toMealy = id
-}
toMealy :: Moore s i (i -> o) -> Mealy s i o
toMealy Moore {..} =
  Mealy { initT = init, deltaT = \s i -> (delta s i, out s i) }

toMoore :: Mealy s i o -> Moore s i (i -> o)
toMoore Mealy {..} = Moore { init  = initT
                           , out   = \s -> snd . deltaT s
                           , delta = \s i -> fst (deltaT s i)
                           }
