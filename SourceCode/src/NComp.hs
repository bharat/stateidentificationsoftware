module NComp
  ( start
  ) where

import qualified Aut
import qualified AutomataDot
import qualified Control.Monad.Extra           as MonadE
import qualified Data.List                     as List
import           Data.Set                      as Set
                                                ( Set )
import qualified DistGraphConstruction
import qualified InputSelection
import           SplitGraph                     ( Evidence
                                                , SplitGraph
                                                )
import qualified Statistics
import           SuspAut                        ( SuspAut
                                                , SuspState
                                                )
import qualified System.Directory              as Directory
import qualified System.Environment            as Env

start :: IO ()
start = do
  maybeArgs <- parseArg
  case maybeArgs of
    (Nothing, _) -> return ()
    (Just caseStudy, Nothing) -> computeAndPrint caseStudy
    (Just caseStudy, Just "compute") -> computeAndPrint caseStudy
    (Just caseStudy, Just "aut-table") -> getAutTable caseStudy
    (Just caseStudy, Just "exp-table") -> getExp caseStudy True
    (Just caseStudy, Just "exp-text") -> getExp caseStudy False
    _ -> error "Should be unreachable."

computeAndPrint :: String -> IO ()
computeAndPrint caseStudy =
  compute caseStudy >>= putStrLn . Statistics.getStatistics

getAutTable :: String -> IO ()
getAutTable caseStudy = do
  maybeAut <- readSavedAutomaton caseStudy
  case maybeAut of
    Nothing ->
      putStrLn
        $  "Automaton of "
        ++ caseStudy
        ++ " has not been stored yet, please run `compute' first"
    Just aut ->
      putStrLn $ Statistics.getAutTable caseStudy (isInput caseStudy) aut

getExp :: String -> Bool -> IO ()
getExp caseStudy getTable = do
  mayberes <- readSavedExpResults caseStudy
  case mayberes of
    Nothing ->
      putStrLn
        $  "Distinguishing graph for "
        ++ caseStudy
        ++ " has not been stored yet, please run `compute' first"
    Just res -> if getTable
      then putStrLn $ Statistics.getExpTable caseStudy res
      else putStrLn $ Statistics.getStatistics res

parseArg :: IO (Maybe String, Maybe String)
parseArg = do
  let caseStudies =
        [ "TCP"
        , "VM-000"
        , "VM-121"
        , "VM-231"
        , "LE-3Center"
        , "LE-3Side"
        , "LE-4"
        , "Dropbox-3"
        , "Dropbox-4"
        , "ESM-IISl"
        , "ESM-IISR"
        , "ESM-IISSl"
        , "ESM-IISL"
        , "ESM-IE"
        ]
  let commands = ["compute", "aut-table", "exp-table", "exp-text"]
  let usage =
        "Usage: adg-exe CASESTUDY [COMMAND]"
          ++ "\nCASESTUDY ::= "
          ++ List.intercalate " | " caseStudies
          ++ "\nCOMMAND ::= "
          ++ List.intercalate " | " commands
  args <- Env.getArgs
  if List.null args || elem (head args) ["help", "--help", "-h"]
    then do
      putStrLn usage
      return (Nothing, Nothing)
    else do
      let arg = head args
      if arg `elem` caseStudies
        then if List.null $ tail args
          then return (Just arg, Nothing)
          else do
            let command = args List.!! 1
            if command `elem` commands
              then return (Just arg, Just command)
              else do
                putStrLn $ command ++ " is not a command!\n" ++ usage
                return (Nothing, Nothing)
        else do
          putStrLn $ arg ++ " is not an available case study!\n" ++ usage
          return (Nothing, Nothing)

compute
  :: String
  -> IO
       ( SuspAut
       , Set (SuspState, SuspState)
       , SplitGraph Int String
       , Evidence String
       , Set (Set SuspState)
       )
compute caseStudy = do
  let dotFileName = if "ESM" `List.isPrefixOf` caseStudy
        then "ESM.dot"
        else caseStudy ++ ".dot"
      getAutFromDot = if "ESM" `List.isPrefixOf` caseStudy
        then AutomataDot.getSuspAutFromMealy
          (isInput caseStudy)
          (InputSelection.getAlfSelection caseStudy)
          "Oquiescence"
        else AutomataDot.getSuspAutFromLTS (isInput caseStudy)
  MonadE.unlessM (Directory.doesDirectoryExist caseStudy)
    $ Directory.createDirectory caseStudy -- create directory if non-existent
  spec <- DistGraphConstruction.getAutomaton "CaseStudyDots/"
                                             dotFileName
                                             (caseStudy ++ "/")
                                             caseStudy
                                             getAutFromDot
                                             False
  (adg, nonInj, graph, comp) <- DistGraphConstruction.getDistGraph
    (caseStudy ++ "/")
    caseStudy
    spec
    False
    False
    False
    False
    True
    (Aut.states spec)
  return (spec, comp, graph, adg, nonInj)

isInput :: String -> (String -> Bool)
isInput caseStudy | "ESM" `List.isPrefixOf` caseStudy = \mu -> head mu == 'I'
                  | caseStudy == "TCP" = \mu -> List.last mu == '?'
                  | "LE" `List.isPrefixOf` caseStudy  = List.isPrefixOf "send"
                  | otherwise                         = List.isPrefixOf "In"

readSavedAutomaton :: String -> IO (Maybe SuspAut)
readSavedAutomaton caseStudy = do
  exists <-
    Directory.doesFileExist
    $  (caseStudy ++ "/")
    ++ "Automaton"
    ++ caseStudy
    ++ ".serialized"
  if exists
    then do
      aut <-
        DistGraphConstruction.readSerializedFile (caseStudy ++ "/")
        $  "Automaton"
        ++ caseStudy
      return $ Just aut
    else return Nothing

readSavedExpResults
  :: String
  -> IO
       ( Maybe
           ( SuspAut
           , Set (SuspState, SuspState)
           , SplitGraph Int String
           , Evidence String
           , Set (Set SuspState)
           )
       )
readSavedExpResults caseStudy = do
  exists <-
    Directory.doesFileExist
    $  (caseStudy ++ "/")
    ++ "ADG"
    ++ caseStudy
    ++ ".serialized"
  if exists
    then do
      spec <-
        DistGraphConstruction.readSerializedFile (caseStudy ++ "/")
        $  "Automaton"
        ++ caseStudy
      (adg, nonInj, graph, comp) <- DistGraphConstruction.getDistGraph
        (caseStudy ++ "/")
        caseStudy
        spec
        True
        True
        True
        False
        True
        (Aut.states spec)
      return $ Just (spec, comp, graph, adg, nonInj)
    else return Nothing
