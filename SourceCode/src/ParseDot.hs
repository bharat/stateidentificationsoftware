{-# LANGUAGE RecordWildCards, OverloadedStrings #-}

module ParseDot
  ( parseDot
  , parseDotBS
  , Statement(..)
  , DotGraph
  , statements
  ) where

{- Author: Joshua Moerman -}
import           Control.Applicative
import           Data.Attoparsec.ByteString.Char8
import           Data.ByteString         hiding ( concat )
import qualified Data.Functor

data DotGraph = DotGraph
  { strict     :: Bool
  , directed   :: Bool
  , name       :: Maybe Identifier
  , statements :: [Statement]
  }
  deriving (Show, Eq, Ord)

parseDot :: Parser DotGraph
parseDot = do
  strict   <- option False (True Data.Functor.<$ stringCI "strict")
  directed <-
    skipSpace
      *> (   (False Data.Functor.<$ stringCI "graph")
         <|> (True Data.Functor.<$ stringCI "digraph")
         )
  name       <- skipSpace *> option Nothing (Just <$> identifier)
  statements <- skipSpace *> parseStatements
  return DotGraph { .. }

parseDotBS :: ByteString -> Either String DotGraph
parseDotBS = parseOnly (skipSpace *> parseDot <* skipSpace <* endOfInput)

type Identifier = ByteString

identifier0 :: Parser Identifier
identifier0 =
  takeWhile1 (\c -> isDigit c || isAlpha_ascii c || c == '_' || c == '.')

identifier :: Parser Identifier
identifier = (char '"' *> takeTill (== '"') <* char '"') <|> identifier0

data Statement = N Identifier [Attribute]
               | E Identifier Identifier [Attribute]
               | A Identifier Identifier
  deriving (Show, Eq, Ord)

parseStatements :: Parser [Statement]
parseStatements = char '{' *> many pstmSemi <* skipSpace <* char '}'
 where
  pstmSemi = skipSpace *> parseStatement <* option ';' (skipSpace *> char ';')

parseStatement :: Parser Statement
parseStatement = do
  node <- identifier <* skipSpace
  do
      E node
        <$> (edgeop *> skipSpace *> identifier)
        <*> (skipSpace *> parseAttributes)
    <|> (A node <$> (char '=' *> skipSpace *> identifier))
    <|> (N node <$> parseAttributes)
  where edgeop = char '-' *> (char '>' <|> char '-')

-- An attribute is just a kay-value pair
type Attribute = (Identifier, Identifier)

-- It is allowed to have multiple attribute lists...
-- For example, n1 -> n2 [label="bla"] [color="green"]
parseAttributes :: Parser [Attribute]
parseAttributes = concat <$> (skipSpace *> many (skipSpace *> pAttrs))
 where
  pAttrs =
    char '['
      *> many
           (skipSpace *> parseAttribute <* optional
             (skipSpace *> (char ';' <|> char ','))
           )
      <* skipSpace
      <* char ']'

parseAttribute :: Parser Attribute
parseAttribute =
  (,) <$> identifier <*> (skipSpace *> char '=' *> skipSpace *> identifier)
